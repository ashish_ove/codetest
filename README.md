# README

# Interview Questions

1. What's the difference between `include` and `extend`?

=> Include :- Includes all the methods of the class as the instance methods.
			  Can be used for multiple inheritance.
Extend :- Includes all the methods of the class as class methods.

2. What's the difference between `clone` and `dup`?

=> Clone :- 

	a. Cloning the active record make exact copy of that active record. Even primary key is shared.
	b. If we will make changes in cloned object, it will reflect the changes to the original one.
	c. When we use freeze method and then clones any array, the cloned array will not allow any other element to be inserted, means it will carry the state of original object.

Dup :-

	a. Using dup in the active record, makes a copy of the active record, but doesn't have any direct connection with the original one. 
	b. If any changes made in the dup object, will not reflect any change in the original object.
	c. It will allow other elemnts to be inserted, even after freeze is used in the original array.

3. What's the difference between `Proc` and `lambda`?

=> Proc :- 
	
	a. It does't give error if wrong number of arguments are given.


Lamda :-

	a. It gives error when wrong number of arguments are given.

4. What's a good example for using `yield` in ruby?

=> Best example of yield can be seen in application.html.erb, where all the views is executed at that place where yield is called.

5. You need to integrate with several REST APIs that often change over time. What design pattern would you use so that it limits  the effect of these changes on your code base?

=> We will use the structure like "api/v1" and for second version we will inherit the "api/v2" from "api/v1" and override the REST APIs that we need.

6. If you were to give a junior dev 3 programming books, what would they be?

7. You've been tasked with adding a new screen that shows an index page of Recent Articles, briefly describe the steps you would take when adding this new feature

=> Following are the steps:-
	a. Create route for the action.
	b. Create action(method) in controller.
	c. Inside action fetch all articles which are created today.
	d. Make a corresponding view for the action.
	e. Make a html table in the view, loop over the instance variable and show the list of recent articles.

# Coding Test

Attached is some base code for the previous question. Use this to answer the following questions. As you work through the problem commit your code referencing each question. e.g. `test-1`, `test-2`.

You can seed some data using `rake db:seed` and navigate to http://127.0.0.1/articles to see how it looks.

When finished, you can package up your code including the .git directory or share via Github.

## Instructions

1. Write a feature test for the articles index page that checks that an article is displayed correctly `spec/features/recent_articles_spec.rb`

2. Write a feature test to ensure filtering by publishing status is correct

3. Based on the search parameters in ArticlesController#index, add a migration to add some indexes

4. Update the model/migrations so that the User is unique by e-mail address and add a test to `spec/models/user_spec.rb`

5. Refactor the ArticlesController#index method so that it is more readable and uses scopes.

6. Remove all N+1 queries from the controller and views

7. The views have a lot of code in it, refactor using view helpers or decorators.

8. Complete the rake task: `lib/tasks/report.rake` that uses an SQL query to extract the following data as a CSV file:
``` csv
Author,Day,Total Drafts,Total Published
Clara Oswald,2019-01-01,20,35
```

9. Complete the rake task: `lib/tasks/import.rake` that imports the file articles.csv into the database efficiently.
