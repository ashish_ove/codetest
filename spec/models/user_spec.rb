require 'rails_helper'

RSpec.describe User, type: :model do

  describe "user" do

  	it "should create user without error" do 
  		user = User.create(name: "testing")
  	end

  	it "should return error when user try to register with registered email" do 
  		user1 = User.create(name: "testing", email: "test@test.com")
  		user2 = User.create(name: "testing", email: "test@test.com")
  		expect(user2).not_to be_valid
  	end
    
  end
end
