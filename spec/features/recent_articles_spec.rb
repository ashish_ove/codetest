require "rails_helper"

RSpec.feature "Widget management", :type => :feature do
  scenario "Correctly displays Articles" do
    visit "/articles"
    expect(page).to have_content('Article 99')
  end

  scenario "Correctly filters Articles by publishing status" do
    visit "/articles?status=published"
    expect(page).not_to have_content("draft")
  end
end