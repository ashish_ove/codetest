require 'rails_helper'

RSpec.describe ArticlesController, type: :controller do
	describe "GET index" do
		before(:all) do
			@user = User.create name: "Clara Oswald", email: "clara@alkhemy.co"
		end
		it "returns a successful response" do

			@article = Article.create(title: "Article 1", content: "Dummy Content", published_at: (Time.now - 3.days), author_id: @user.id)

		  get :index
			expect(response.code).to eq('200')
		end
	end
end
