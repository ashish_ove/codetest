class AddIndexInarticles < ActiveRecord::Migration[5.2]
  def change
  	add_index :articles, :published_at
  	add_index :articles, :title
  end
end
