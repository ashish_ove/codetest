class Article < ApplicationRecord
  belongs_to :author, class_name: User.name

  # scope :recent, -> { order(:updated_at, :desc) }
   scope :last_seven_day_articles, -> { where("created_at >= ?", Time.now - 7.days).order("updated_at DESC")}

   scope :search_by_title, ->(title_search) { where("title like ?", title_search) } 
   scope :search_by_author, ->(author_id) { Article.includes(:authors).where("author_id like ?", author_id) } 
   scope :published, -> { where("published_at IS NOT NULL AND published_at < ?", Time.now)}
   scope :draft, -> {where("published_at IS NULL OR published_at >= ?", Time.now)}
end
