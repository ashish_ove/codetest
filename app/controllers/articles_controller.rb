class ArticlesController < ApplicationController
  include Pagy::Backend
  include ArticlesHelper

  def index
    @articles = Article.last_seven_day_articles
    @articles = Article.search_by_title(params[:title]) unless params[:title].blank?
    @articles = Article.search_by_author(params[:author_id]) unless params[:author_id].blank?
    @recent_authors = @articles.includes(:author).collect {|article| article.author.name }.uniq.join(", ")
    
    if params[:status] == "published"
      @articles = Article.published
    elsif params[:status] == "draft"
      @articles = Article.draft
    end
    @pagy, @articles = pagy(@articles)
  end
end


