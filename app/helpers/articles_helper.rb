module ArticlesHelper
  include Pagy::Frontend

  def publish_date(published_at)
    published_at.strftime("%Y-%m-%d")
  end

  def article_status(published_at)
  	(published_at.nil? || published_at > Time.now) ? "draft" : "published"
  end
end
