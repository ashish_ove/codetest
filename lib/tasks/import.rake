require 'csv'

desc "Import Articles"
namespace :import do
  task :articles => :environment do
    # code here
		CSV.foreach('articles.csv', :headers => true) do |row|
		  Article.create({
		    title: row[0],
		    author: User.find_by(name: row[1]),
		    published_at: row[2],
		    content: row[3]
		  })
		end
  end
end
