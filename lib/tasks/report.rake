require 'csv'

desc "Create an author report"
namespace :report do
  task :authors => :environment do
    # Your query here
    @articles = Article.includes(:author)
    
    # sql_query = 'SELECT * FROM articles INNER JOIN "articles" "users" ON "users"."id" = "articles"."author_id"'
    # @articles = ActiveRecord::Base.connection.execute(sql_query).to_a
    CSV.open('report.csv', "w") do |csv|
      csv << ["Author", "Day", "Total Drafts", "Total Published"];
      @articles.each_with_index do |l,index|
        csv << [l.author.name, l.published_at.try(:strftime, "%Y-%m-%d")]
      end

      # your code here
    end
  end
end

 